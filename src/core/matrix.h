
#pragma once

#include <algorithm>
#include <initializer_list>
#include <ostream>
#include <array>

namespace core
{
	template <unsigned int D, typename T>
	class Mat
	{
	public:
		Mat() { std::fill(raw.begin(), raw.end(), T(0)); }
		Mat(const std::initializer_list<T> &list)  { std::move(list.begin(), list.end(), raw.begin()); }

		T& at(unsigned int collumn, unsigned int row) { return raw.at(D*collumn + row); }
		const T& at(unsigned int collumn, unsigned int row) const { return raw.at(D*collumn + row); }

		unsigned int dim() const { return D; }

		const Mat<D, T> operator +(const Mat<D, T> &rhs) const;
		const Mat<D, T> operator -(const Mat<D, T> &rhs) const;
		const Mat<D, T> operator *(const Mat<D, T> &rhs) const;
		const Vec<D, T> operator *(const Vec<D, T> &rhs) const;
		const Mat<D, T> operator *(const T &value) const;
	private:
		std::array<T, D*D> raw;
	};

	template <unsigned int D, typename T>
	const Mat<D, T> Mat<D, T>::operator +(const Mat<D, T> &rhs) const
	{
		if (this->dim() != rhs.dim())
			throw std::runtime_error("Error: operation demands two matrices of same size");
		Mat<D, T> result;
		std::transform(this->raw.begin(), this->raw.end(), rhs.raw.begin(), result.raw.begin(), std::plus<T>());

		return result;
	}

	template <unsigned int D, typename T>
	const Mat<D, T> Mat<D, T>::operator -(const Mat<D, T> &rhs) const
	{
		if (this->dim() != rhs.dim())
			throw std::runtime_error("Error: operation demands two matrices of same size");
		Mat<D, T> result;
		std::transform(this->raw.begin(), this->raw.end(), rhs.raw.begin(), result.raw.begin(), std::minus<T>());

		return result;
	}

	template <unsigned int D, typename T>
	const Mat<D, T> Mat<D, T>::operator *(const Mat<D, T> &rhs) const
	{
		if (this->dim() != rhs.dim())
			throw std::runtime_error("Error: operation demands two matrices of same size");
		Mat<D,T> result;
		for (unsigned int i = 0; i < D; ++i)
			for (unsigned int j = 0; j < D; ++j)
			{
				auto &value = result.at(i, j);
				for (unsigned int scan = 0; scan < D; ++scan)
					value += this->at(i, scan) * rhs.at(scan, j); // TODO: more effective code, direct access
			}

		return result;
	}

	template <unsigned int D, typename T>
	const Vec<D, T> Mat<D, T>::operator *(const Vec<D, T> &vec) const
	{
		if (this->dim() != vec.dim())
			throw std::runtime_error("Error: operation demands two matrices of same size");
		Vec<D, T> result;
		for (unsigned int i = 0; i < D; ++i)
		{
			auto &value = result.at(i);
			for (unsigned int scan = 0; scan < D; ++scan)
				value += this->at(i, scan) * vec.at(scan); // TODO: more effective code, direct access
		}

		return result;
	}

	template <unsigned int D, typename T>
    const Mat<D, T> Mat<D, T>::operator *(const T &value) const
	{
		Mat<D, T> result;
		for (auto index = 0; index < D*D; ++index)
			result.raw[index] = this->raw[index] * value;

		return result;
	}

	template <unsigned int D, typename T>
	std::ostream& operator<<(std::ostream& stream, const Mat<D, T>& vec)
	{
		for (unsigned int i = 0; i < D; ++i)
		{
			stream << "|";
			for (unsigned int j = 0; j < D; ++j)
			{
				if (j != 0) stream << " ";
				stream << vec.at(i,j);
			}
			std::cout << "|" << std::endl;
		}
		return stream;
	}


	using Mat3f = Mat<3, float>;
	using Mat3i = Mat<3, int>;
	using Mat2f = Mat<2, float>;
	using Mat2f = Mat<2, float>;

	template<typename T>
    Mat<2, T> inverse(const Mat<2, T> &mat)
	{
		const T D = mat.at(0, 0)*mat.at(1, 1) - mat.at(1, 0)*mat.at(0, 1);
		return{ mat.at(1,1)/D, -mat.at(0,1)/D, -mat.at(1,0)/D, mat.at(0,0)/D };
	}
}
