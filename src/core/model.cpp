#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include "rasterizer.h"
#include "model.h"

using namespace core;

Model::Model()
{
}


Model::~Model()
{
}

void Model::clear()
{
	_vertices.clear();
	_normals.clear();
	_texCoords.clear();
	_conveyer.clear();
        
        flagNormals = false;
        flagTextures = false;
}

void Model::registerVertex(const Vec3f &vertex)
{
	_vertices.push_back(vertex);
}

void Model::registerTexCoord(const Vec3f &texCoord)
{
	_texCoords.push_back(texCoord);
}

void Model::registerNormal(const Vec3f &normal)
{
	_normals.push_back(normal);
}

void Model::registerFace(const std::vector<VertexInfo> &face)
{
    if(face.size()!= 3)
        throw std::runtime_error("Error: face size need exactly 3 vertices.");
    _conveyer.push_back(face);
}

// If model is too big, renormalizes it to [-1,1] range
void Model::normalize()
{
    core::Vec3f max;
    for(const auto &vertex: _vertices)
    {
        max[0] = std::max(std::abs(vertex[0]), max[0]);
        max[1] = std::max(std::abs(vertex[1]), max[1]);
        max[2] = std::max(std::abs(vertex[2]), max[2]);
    }
    
    const float factor = 1.0f / (std::max(max[0], std::max(max[1],max[2])));
    
    for(auto &vertex: _vertices)
        vertex = vertex * factor;
}

void Model::draw(buffer::Buffer &buffer, buffer::ZBuffer &zbuffer)
{
	const Vec3f lightDirection = Vec3f{ 0.0f, 0.0f, 1.0f }.normalize();
	const float ambientLight = 0.5f;

	for ( auto &face : _conveyer)
	{

		const Vec3f &v1 = _vertices.at(face.at(0).vertex);
		const Vec3f &v2 = _vertices.at(face.at(1).vertex);
		const Vec3f &v3 = _vertices.at(face.at(2).vertex);

		const Vec3f &t1 = _texCoords.at(face.at(0).texCoord);
		const Vec3f &t2 = _texCoords.at(face.at(1).texCoord);
		const Vec3f &t3 = _texCoords.at(face.at(2).texCoord);

		Vec3f normal;
		if (!flagNormals)
		    normal = multiply(v2 - v1, v3 - v1).normalize();
		else
            normal = (_vertices.at(face.at(0).normal) +
                      _vertices.at(face.at(1).normal) +
                      _vertices.at(face.at(2).normal))*(1.0f/3.0f);
                
		float diffuseLight = normal * lightDirection;
		if (diffuseLight < 0.0f)
			diffuseLight = 0.0f;

		const float lightIntensity = ambientLight +diffuseLight*(1.0f-ambientLight);

		const Vec3i pv1 = { int((v1[0] + 1.)*buffer.get_width() / 2.), int((v1[1] + 1.)*buffer.get_height() / 2.), int(v1[2] * 10000) };
		const Vec3i pv2 = { int((v2[0] + 1.)*buffer.get_width() / 2.), int((v2[1] + 1.)*buffer.get_height() / 2.), int(v2[2] * 10000) };
		const Vec3i pv3 = { int((v3[0] + 1.)*buffer.get_width() / 2.), int((v3[1] + 1.)*buffer.get_height() / 2.), int(v3[2] * 10000) };

		Rasterizer::zt_fill(pv1, pv2, pv3, t1, t2, t3, buffer, zbuffer, this->_texture, lightIntensity);
	}
}


// --- Loading from OBJ format ---

std::vector<std::string> split(const std::string &text, char sep) {
  std::vector<std::string> tokens;
  int start = 0, end = 0;
  while ((end = text.find(sep, start)) != std::string::npos) {
      
    tokens.push_back(text.substr(start, end - start));
    start = end + 1;
  }
  tokens.push_back(text.substr(start));
  
  return tokens;
}

template<typename T>
const T convert(const std::string text)
{
    std::stringstream ss(text);
    T result;
    ss >> result;
    
    return result;
}

const VertexInfo parseVertexInfo(const std::string &text)
{
    std::vector<std::string> tokens = split(text,'/');
    //if(tokens.size() != 3)
    //    throw std::runtime_error("Error: wrong format on obj file");
    
    VertexInfo result;
    
	if (tokens.size() >= 1 && !tokens.at(0).empty())
        result.vertex = convert<int>(tokens.at(0)) - 1; // OBJ index begins from 1
    
	if (tokens.size() >= 2 && !tokens.at(1).empty())
        result.texCoord = convert<int>(tokens.at(1)) - 1;
    
	if (tokens.size() >= 3 && !tokens.at(2).empty())
        result.normal = convert<int>(tokens.at(2)) - 1;
    
    return result;
}

const std::vector<VertexInfo> parseFace(const std::string &text)
{
        std::vector<VertexInfo> result;
        std::stringstream ss(text);
        
        std::string vectorInfoStr;
        ss >> vectorInfoStr; // Removing first "f "
        for(unsigned int i = 0; i < 3; i++)
        {
            ss >> vectorInfoStr;
            result.push_back(parseVertexInfo(vectorInfoStr));
        }
        
        if(result.size() != 3)
            throw std::runtime_error("Error: wrong format on obj file");
        
        return  result;
}

const Vec3f parseVector(const std::string& text)
{
        Vec3f result;
        std::stringstream ss(text);
        
        std::string coordInfoStr;
        ss >> coordInfoStr; // Removing first "n "
        for(unsigned int i = 0; i < 3; i++)
        {
            ss >> coordInfoStr;
            result[i] = convert<float>(coordInfoStr);
        }
        
        return  result;
}

bool compareType(const std::string& text, const std::string &type)
{
    std::stringstream ss(text);
    std::string typeInfoStr;
    ss >> typeInfoStr;
    
    return (typeInfoStr == type);
}

void Model::loadWavefrontObj(const std::string& filename)
{
	clear();

	std::ifstream in;
	in.open(filename, std::ifstream::in);
	if (in.fail())
	{
		std::cout << "Error: can't load object" << std::endl;
		return;
	}

	std::string line;
	while (!in.eof()) {
		std::getline(in, line);
		std::istringstream iss(line.c_str());
		if (compareType(line, "v")) {
			registerVertex(parseVector(line));
		}
		else if (compareType(line, "vt")) {
			registerTexCoord(parseVector(line));
            flagTextures = true;
		}
		else if (compareType(line, "vn")) {
			registerNormal(parseVector(line));
            flagNormals = true;
		}     
		else if (compareType(line, "f")) {
            registerFace(parseFace(line));
		}
	}
        
    if(!flagNormals)
       std::cout << "Model do not provide normals data, render will use autogenerated normals" << std::endl;

	if (!flagTextures)
		std::cout << "Model do not provide texture data..." << std::endl;

        
	std::cout << "Model loaded: " << std::endl << 
					 " * " << _conveyer.size() << " faces," << std::endl <<
					 " * " << _vertices.size() << " vertices," << std::endl <<
					 " * " << _texCoords.size() << " texture coordinates," << std::endl <<
					 " * " << _normals.size() << " normals" << std::endl;
}

void Model::loadTexture(const std::string& filename)
{
	bool succeed = _texture.read_tga_file(filename.c_str());
	if (!succeed)
		std::cout << "Loading texture failed" << std::endl;

	_texture.flip_vertically(); // TODO: this seems to be hack. Leaving now as is.

	std::cout << "Texture loaded: " << std::endl <<
		" * Width : " << _texture.get_width() << std::endl <<
		" * Height: " << _texture.get_height() << std::endl;
}