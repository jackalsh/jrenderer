#pragma once

#include "geometry.h"
#include "../buffer/tgaimage.h"
#include "../buffer/depthbuffer.h"

namespace core
{
	class Rasterizer
	{
	public:
		static void point(Vec2i position, buffer::Buffer &buffer, const buffer::Color &color);
		static void line(Vec2i from, Vec2i to, buffer::Buffer &buffer, const buffer::Color &color);
		static void fill(Vec2i v1, Vec2i v2, Vec2i v3, buffer::Buffer &buffer, const buffer::Color &color);
		static void z_fill(Vec3i v1, Vec3i v2, Vec3i v3, buffer::Buffer &buffer, buffer::ZBuffer &zbuffer, const buffer::Color &color);
		static void zt_fill(Vec3i v1, Vec3i v2, Vec3i v3, Vec3f t1, Vec3f t2, Vec3f t3, buffer::Buffer &buffer, buffer::ZBuffer &zbuffer, const buffer::TGAImage &texture, const float intensityLight);
	};

}
