
#pragma once

#include <algorithm>
#include <initializer_list>
#include <ostream>
#include <array>

namespace core
{
	template <unsigned int D, typename T>
	class Vec
	{
	public:
		Vec() { std::fill(raw.begin(), raw.end(), T(0)); }
		Vec(const std::initializer_list<T> &list)  { std::move(list.begin(), list.end(), raw.begin()); }

		T& at(unsigned int dim) { return raw.at(dim); }
		const T& at(unsigned int dim) const { return raw.at(dim); }
		T& operator[](unsigned int dim) { return raw[dim]; }
		const T& operator[](unsigned int dim) const { return raw[dim]; }

		unsigned int dim() const { return D; }
		const T norm() const;

		Vec<D, T> operator +(const Vec<D, T> &rhs) const;
		Vec<D, T> operator -(const Vec<D, T> &rhs) const;
		const T operator *(const Vec<D, T> &rhs) const;
                const Vec<D, T> operator *(const T &value) const;
		Vec<D, T> normalize() const;
	private:
		std::array<T, D> raw;
	};

	template <unsigned int D, typename T>
	Vec<D, T> Vec<D, T>::operator +(const Vec<D, T> &rhs) const
	{
		if (this->dim() != rhs.dim())
			throw std::runtime_error("Error: operation demands two vectors of same size");
		Vec<D, T> result;
		std::transform(this->raw.begin(), this->raw.end(), rhs.raw.begin(), result.raw.begin(), std::plus<T>());

		return result;
	}

	template <unsigned int D, typename T>
	Vec<D, T> Vec<D, T>::operator -(const Vec<D, T> &rhs) const
	{
		if (this->dim() != rhs.dim())
			throw std::runtime_error("Error: operation demands two vectors of same size");
		Vec<D, T> result;
		std::transform(this->raw.begin(), this->raw.end(), rhs.raw.begin(), result.raw.begin(), std::minus<T>());

		return result;
	}

	template <unsigned int D, typename T>
	const T Vec<D, T>::operator *(const Vec<D, T> &rhs) const
	{
		if (this->dim() != rhs.dim())
			throw std::runtime_error("Error: operation demands two vectors of same size");
		T result = T(0);
		for (unsigned int index = 0; index < D; ++index)
			result += this->raw[index] * rhs.raw[index];

		return result;
	}
        
        template <unsigned int D, typename T>
        const Vec<D, T> Vec<D, T>::operator *(const T &value) const
        {
            Vec<D, T> result;
            for (unsigned int index = 0; index < D; ++index)
			result[index] = this->raw[index] * value;
            
            return result;
        }

	template <unsigned int D, typename T>
	const T  Vec<D, T>::norm() const
	{
		return (sqrt(((*this)*(*this))));
	}

	template <unsigned int D, typename T>
	Vec<D, T> Vec<D, T>::normalize() const
	{
		T norm = this->norm();

		Vec<D, T> result;
		for (unsigned int index = 0; index < D; ++index)
			result.raw[index] = this->raw[index] / norm;

		return result;
	}

	template <unsigned int D, typename T>
	std::ostream& operator<<(std::ostream& stream, const Vec<D, T>& vec)
	{
		stream << "(";
		for (unsigned int index = 0; index < D; ++index)
		{
			if (index != 0) stream << " ";
			stream << vec.at(index);
		}
		stream << ")";
		return stream;
	}

	using Vec2i = Vec<2, int>;
	using Vec3i = Vec<3, int>;
	using Vec2f = Vec<2, float>;
	using Vec3f = Vec<3, float>;

	// Function of vector multiplication is usefull only when D=3
	// and otherwise is very coplicated(determinants...)
	template<typename T>
	const Vec<3, T> multiply(const Vec<3, T> &lhs, const Vec<3, T> &rhs)
	{
		return{ lhs[1] * rhs[2] - lhs[2] * rhs[1], lhs[2] * rhs[0] - lhs[0] * rhs[2], lhs[0] * rhs[1] - lhs[1] * rhs[0] };
	}
}