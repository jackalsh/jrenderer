#pragma once

#include <vector>
#include <list>

#include "../buffer/tgaimage.h"
#include "../buffer/depthbuffer.h"
#include "geometry.h"

namespace core
{
	class VertexInfo
	{
	public:
		VertexInfo(unsigned int vertex_id, unsigned int normal_id, unsigned int texCoord_id) : vertex(vertex_id), normal(normal_id), texCoord(texCoord_id) {}
		VertexInfo() : vertex(0), normal(0), texCoord(0) {}

		unsigned int vertex;
		unsigned int texCoord;                
		unsigned int normal;
	private:
	};

	class Model
	{
	public:
		Model();
		~Model();

		void clear();

		void registerVertex(const Vec3f &vertex);
		void registerTexCoord(const Vec3f &texCoord);                
		void registerNormal(const Vec3f &normal);

		void registerFace(const std::vector<VertexInfo> &face);

		void loadWavefrontObj(const std::string& filename);
		void loadTexture(const std::string& filename);
                
        void normalize(); //Temporary? function to normalize all model coordinates to 0-1 range. 

		void draw(buffer::Buffer &buffer, buffer::ZBuffer &zbuffer);
	private:
		std::vector<Vec3f> _vertices;
		std::vector<Vec3f> _texCoords;                
		std::vector<Vec3f> _normals;

		std::list<std::vector<VertexInfo> > _conveyer;
		buffer::Buffer _texture;
                
                bool flagNormals;
                bool flagTextures;
	};

}
