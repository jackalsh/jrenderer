
#include <iostream>

#include "rasterizer.h"
#include "matrix.h"

using namespace core;

void Rasterizer::point(Vec2i position, buffer::Buffer &buffer, const buffer::Color &color)
{
	buffer.set(position[0], position[1], color);
}

void Rasterizer::line(Vec2i from, Vec2i to, buffer::Buffer &buffer, const buffer::Color &color) {
	// Special cases

	bool flipped = (std::abs(to[1] - from[1]) > std::abs(to[0] - from[0])) ? true : false;
	if (flipped) //Invariant!, after this, dx<dy and no spaces in the line.
	{
		std::swap(from[0], from[1]);
		std::swap(to[0], to[1]);
	}

	if (from[0] > to[0]) // Invariant, after this from.x < to.x by definition
	{
		std::swap(from, to);
	}

	const int dnumerator = std::abs(to[1] - from[1]);
	const int ddenominator = to[0] - from[0];

	int x = from[0]; int y = from[1];
	int error = 0;
	const int up_down_one = (to[1] - from[1] >= 0) ? 1 : -1;

	for (; x <= to[0]; x++)
	{
		error += dnumerator;
		if (error >= ddenominator)
		{
			y += up_down_one;
			error -= ddenominator;
		}

		if (flipped) // Restoring because of first invariant
			buffer.set(y, x, color);
		else
			buffer.set(x, y, color);
	}
}

// Triangles filled with color
void Rasterizer::fill(Vec2i v1, Vec2i v2, Vec2i v3, buffer::Buffer &buffer, const buffer::Color &color)
{
    const Vec3i f12 = { v2[0] - v1[0], v1[1] - v2[1], v1[0] * v2[1] - v1[1] * v2[0] };
	const Vec3i f23 = { v3[0] - v2[0], v2[1] - v3[1], v2[0] * v3[1] - v2[1] * v3[0] };
	const Vec3i f31 = { v1[0] - v3[0], v3[1] - v1[1], v3[0] * v1[1] - v3[1] * v1[0] };

	const Vec2i minBorder = { std::min(std::min(v1[0], v2[0]), v3[0]), std::min(std::min(v1[1], v2[1]), v3[1]) };
	const Vec2i maxBorder = { std::max(std::max(v1[0], v2[0]), v3[0]), std::max(std::max(v1[1], v2[1]), v3[1]) };

	int D1, D2, D3;

	for (int y = minBorder[1]; y < maxBorder[1]; ++y)
		for (int x = minBorder[0]; x < maxBorder[0]; ++x)
			{
				D1 = f12 * Vec3i{ y, x, 1 };
				D2 = f23 * Vec3i{ y, x, 1 };
				D3 = f31 * Vec3i{ y, x, 1 };

				if (!((D1 <= 0 && D2 <= 0 && D3 <= 0) ||  //CR faces
					(D1 >= 0 && D2 >= 0 && D3 >= 0)))   //CCR faces
					continue;

				buffer.set(x,y,color);
			}
}

// Same with z buffer
void Rasterizer::z_fill(Vec3i v1, Vec3i v2, Vec3i v3, buffer::Buffer &buffer, buffer::ZBuffer &zbuffer, const buffer::Color &color)
{
	//std::cout << v2 - v1 << "-" << v3 - v1 << std::endl;

	const Vec3i plane = multiply(v2 - v1, v3 - v1);
	if (plane[2] == 0)
		return; //this face is not seen

	const int D = plane * v1;

	const Vec3i f12 = { v2[0] - v1[0], v1[1] - v2[1], v1[0] * v2[1] - v1[1] * v2[0] };
	const Vec3i f23 = { v3[0] - v2[0], v2[1] - v3[1], v2[0] * v3[1] - v2[1] * v3[0] };
	const Vec3i f31 = { v1[0] - v3[0], v3[1] - v1[1], v3[0] * v1[1] - v3[1] * v1[0] };

	const Vec2i minBorder = { std::min(std::min(v1[0], v2[0]), v3[0]), std::min(std::min(v1[1], v2[1]), v3[1]) };
	const Vec2i maxBorder = { std::max(std::max(v1[0], v2[0]), v3[0]), std::max(std::max(v1[1], v2[1]), v3[1]) };

	int D1, D2, D3;

	for (int y = minBorder[1]; y < maxBorder[1]; ++y)
		for (int x = minBorder[0]; x < maxBorder[0]; ++x)
		{
			D1 = f12 * Vec3i{ y, x, 1 };
			D2 = f23 * Vec3i{ y, x, 1 };
			D3 = f31 * Vec3i{ y, x, 1 };

			if (!((D1 <= 0 && D2 <= 0 && D3 <= 0) ||  //CR faces
				(D1 >= 0 && D2 >= 0 && D3 >= 0)))   //CCR faces
				continue;

			int depth = (D - (plane[1]*y + plane[0]*x))/plane[2];
			
			if (depth >= zbuffer.get(x, y))
			{
				zbuffer.set(x, y, depth);
				buffer.set(x, y, color);
			}
			
		}
}

const Vec2f interpolate(const Vec2i &v, const Vec3i &v1, const Vec3i &v2, const Vec3i &v3, const Vec3f &t1, const Vec3f &t2, const Vec3f &t3)
{
	core::Mat2f interpA = core::inverse<float>(Mat2f{ float(v2[0] - v1[0]), float(v3[0] - v1[0]),
		float(v2[1] - v1[1]), float(v3[1] - v1[1]) });
	core::Mat2f interpB = Mat2f{ float(t2[0] - t1[0]), float(t3[0] - t1[0]),
		float(t2[1] - t1[1]), float(t3[1] - t1[1]) };

	core::Vec2f result = Vec2f{ t1[0], t1[1] }+(interpB*interpA)*Vec2f{ float(v[0] - v1[0]), float(v[1] - v1[1]) };
	
	return result;
}

// With textures
void Rasterizer::zt_fill(Vec3i v1, Vec3i v2, Vec3i v3, Vec3f t1, Vec3f t2, Vec3f t3, buffer::Buffer &buffer, buffer::ZBuffer &zbuffer, const buffer::TGAImage &texture, const float intensityLight)
{
	const Vec3i plane = multiply(v2 - v1, v3 - v1);
	if (plane[2] == 0)
		return; //this face is not seen

	const int D = plane * v1;

	const Vec3i f12 = { v2[0] - v1[0], v1[1] - v2[1], v1[0] * v2[1] - v1[1] * v2[0] };
	const Vec3i f23 = { v3[0] - v2[0], v2[1] - v3[1], v2[0] * v3[1] - v2[1] * v3[0] };
	const Vec3i f31 = { v1[0] - v3[0], v3[1] - v1[1], v3[0] * v1[1] - v3[1] * v1[0] };

	const Vec2i minBorder = { std::min(std::min(v1[0], v2[0]), v3[0]), std::min(std::min(v1[1], v2[1]), v3[1]) };
	const Vec2i maxBorder = { std::max(std::max(v1[0], v2[0]), v3[0]), std::max(std::max(v1[1], v2[1]), v3[1]) };

	int D1, D2, D3;

	for (int y = minBorder[1]; y < maxBorder[1]; ++y)
	for (int x = minBorder[0]; x < maxBorder[0]; ++x)
	{
		D1 = f12 * Vec3i{ y, x, 1 };
		D2 = f23 * Vec3i{ y, x, 1 };
		D3 = f31 * Vec3i{ y, x, 1 };

		if (!((D1 <= 0 && D2 <= 0 && D3 <= 0) ||  //CR faces
			  (D1 >= 0 && D2 >= 0 && D3 >= 0)))   //CCR faces
			continue;

		int depth = (D - (plane[1] * y + plane[0] * x)) / plane[2];

		if (depth >= zbuffer.get(x, y))
		{
			zbuffer.set(x, y, depth);

			const Vec2f t = interpolate({ x, y }, v1, v2, v3, t1, t2, t3);
            buffer::Color color= texture.get( int(t[0]*texture.get_width()), int(t[1]*texture.get_height()));
            color = buffer::Color( int(color.r * intensityLight), int(color.g * intensityLight), int(color.b * intensityLight), 255 );
                        
			buffer.set(x, y, color);
		}

	}
}