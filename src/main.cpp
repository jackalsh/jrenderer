
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <regex>

#include "core/model.h"
#include "core/rasterizer.h"

#include "buffer/tgaimage.h"
#include "buffer/depthbuffer.h"

#include "core/matrix.h"

int main(int argc, char** argv) {
	const unsigned int width = 900, heigth = 900;

	buffer::Buffer buffer(width, heigth, buffer::Buffer::RGB);
	buffer::ZBuffer zbuffer(width, heigth);

	core::Model model;
    std::cout << "Loading..." << std::endl;
	model.loadWavefrontObj("../obj/african_head.obj");
	model.loadTexture("../obj/african_head_diffuse.tga");
        
    std::cout << "Normalizing..." << std::endl;
    model.normalize();

    std::cout << "Drawing..." << std::endl;
	model.draw(buffer, zbuffer);

	buffer.flip_vertically();
	buffer.write_tga_file("output.tga");
        
	return 0;
}
