#pragma once

#include <limits>
#include <stdexcept>
#include <vector>
#include <iostream>

namespace buffer
{
	template <typename T>
	class DepthBuffer
	{
	public:
		DepthBuffer(int w, int h);

	    int getWidth() const { return width; }
	    int getHeigth() const { return heigth; }

		const T get(int x, int y) const;
		void set(int x, int y, const T &value);

		void clear();
	private:
		std::vector<T> raw;
		const int width;
		const int heigth;
	};

	template <typename T>
	DepthBuffer<T>::DepthBuffer(int w, int h)
		: width(w), heigth(h)
	{
		raw.resize(width * heigth);
		clear();
	}

	template <typename T>
	const T DepthBuffer<T>::get(int x, int y) const
	{
		if (x >= getWidth() || y >= getHeigth())
			throw std::runtime_error("Error: coordinate is ouside the buffer");
		unsigned int index = y * getWidth() + x;
		return raw[index];
	}

	template <typename T>
	void DepthBuffer<T>::set(int x, int y, const T &value)
	{
		if (x >= getWidth() || y >= getHeigth())
			throw std::runtime_error("Error: coordinate is ouside the buffer");
		unsigned int index = y * getWidth() + x;
		raw[index] = value;
	}

	template <typename T>
	void DepthBuffer<T>::clear()
	{
		const T min = std::numeric_limits<T>::min();
		std::fill(raw.begin(), raw.end(), min);
	}

	using ZBuffer = DepthBuffer<int>;
}