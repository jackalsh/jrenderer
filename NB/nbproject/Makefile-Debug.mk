#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/417348998/depthbuffer.o \
	${OBJECTDIR}/_ext/417348998/tgaimage.o \
	${OBJECTDIR}/_ext/1019403175/model.o \
	${OBJECTDIR}/_ext/1019403175/rasterizer.o \
	${OBJECTDIR}/_ext/1360937237/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++11
CXXFLAGS=-std=c++11

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/nb

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/nb: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/nb ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/417348998/depthbuffer.o: ../src/buffer/depthbuffer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/417348998
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/417348998/depthbuffer.o ../src/buffer/depthbuffer.cpp

${OBJECTDIR}/_ext/417348998/tgaimage.o: ../src/buffer/tgaimage.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/417348998
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/417348998/tgaimage.o ../src/buffer/tgaimage.cpp

${OBJECTDIR}/_ext/1019403175/model.o: ../src/core/model.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1019403175
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1019403175/model.o ../src/core/model.cpp

${OBJECTDIR}/_ext/1019403175/rasterizer.o: ../src/core/rasterizer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1019403175
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1019403175/rasterizer.o ../src/core/rasterizer.cpp

${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360937237
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/nb

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
